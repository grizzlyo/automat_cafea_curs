package com.sda.coffe.machine.utils;

public interface Consts {
    /*
    cai catre fisiere
     */
    public static final String PATH_TO_PRODUCTS_FILE = "C:\\Users\\vasile\\Desktop\\Proiecte Java\\CoffeMachine\\src\\main\\resources\\products\\products.txt";
    public static final String PATH_TO_STOCK_FILE = "C:\\Users\\vasile\\Desktop\\Proiecte Java\\CoffeMachine\\src\\main\\resources\\stock\\stock.txt";
    public static final String PATH_TO_MONEY_FILE = "C:\\Users\\vasile\\Desktop\\Proiecte Java\\CoffeMachine\\src\\main\\resources\\money\\money.txt";

    /*
    mesaje generale utilizator
     */
    public static final String DISPLAY_STOCK = "1. Afisare stock produse; ";
    public static final String DISPLAY_STOCK_MONEY = "2. Afisare stock bani; ";
    public static final String DISPLAY_PROFIT = "3. Afisare profit; ";
    public static final String DISPLAY_MAKE_COFFEE = "4. Produ cafea; ";
    public static final String DISPLAY_EXIT = "5. Exit! ";
    public static final String DISPLAY_GENERAL_COFFEE = "Bun venit!";
    public static final String DISPLAY_COFFEE_IN_PROGRESS = "Asteptati!!!";
    public static final String DISPLAY_SELECT_OPTION = "Selectati o optiune: ";
    public static final String DISPLAY_EXISTING_OPITION = "Va rugam inserati o optiune valida!";
    public static final String DISPLAY_COFFEE_FINISHED = "Cafeaua este gata!";
    public static final String DISPLAY_OUT_OF_STOCK = "Nu avem suficiente produse pentru a reliza cafeaua!";
    public static final String DISPLAY_INSERT_MONEY_MESSAGE = "Inserati bani:";
    public static final String DISPLAY_INSERT_10_BANI = "Cate monede de 10 bani inserati?";
    public static final String DISPLAY_INSERT_50_BANI = "Cate monede de 50 bani inserati?";
    public static final String DISPLAY_INSERT_1_LEU = "Cate bancnote de 1 leu inserati?";
    public static final String DISPLAY_INSERT_5_LEI = "Cate bancnote de 5 lei inserati?";
    public static final String DISPLAY_REST_MESSAGE = "Acesta este restul: ";
    public static final String DISPLAY_COFFE_PRICE = "Cafeaua costa 2 lei: ";
    public static final String DISPLAY_PROFIT_VALUE = "Profitul este: ";



}
