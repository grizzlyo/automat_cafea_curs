package com.sda.coffe.machine.display;

import com.sda.coffe.machine.money.Money;
import com.sda.coffe.machine.products.Products;

import java.util.List;
import java.util.Map;

public class DisplayData {

    public static void displayMessage (String message){
        System.out.println(message);
    }

    public static void newLine(){
        System.out.println();
    }

    public static void displayStock(Map<Products, Integer> stock){
        for (Map.Entry<Products, Integer> entry : stock.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    public static void displayMoneyStock(List<Money> moneyList){
        for (Money money : moneyList){
            System.out.println(money);
        }
    }

}
