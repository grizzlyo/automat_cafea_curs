package com.sda.coffe.machine.products;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class ProductsReader {

    final static Logger LOGGER = Logger.getLogger(ProductsReader.class);

    public List<Products> readProductsFromFile(String path) {

        LOGGER.info("Entering in method: readProductsFromFile with path: " + path);

        List<Products> allProducts = new LinkedList<>();

        File file = new File(path);

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);

            String line = reader.readLine();

            while (line != null) {
                String[] elements = line.split(" ");

                Products products = new Products(elements[0], Integer.parseInt(elements[1]), Integer.valueOf(elements[2]));

                allProducts.add(products);

                line = reader.readLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.error(e);
        } catch (IOException ex) {
            ex.printStackTrace();
            LOGGER.error(ex);
        }

        LOGGER.info("Method readProductsFromFile have the following output: " + allProducts);

        return allProducts;
    }

}
