
package com.sda.coffe.machine.stock;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class ProductStockReader {

    final static Logger LOGGER = Logger.getLogger(ProductStockReader.class);

    public List<Integer> readQuantityFromFile(String path) {
        LOGGER.info("Entering in method: readQuantityFromFile with path " + path);
        List<Integer> quantity = new LinkedList<>();

        File file = new File(path);

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);

            String line = reader.readLine();
            String[] elements = line.split(" ");

            for (String element : elements) {
                quantity.add(Integer.valueOf(element));
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.error(e);
        } catch (IOException ex) {
            ex.printStackTrace();
            LOGGER.error(ex);
        }

        LOGGER.info("Method readQuantityFromFile have the following output: " + quantity);

        return quantity;
    }
}
