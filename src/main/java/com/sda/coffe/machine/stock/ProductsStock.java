package com.sda.coffe.machine.stock;

import com.sda.coffe.machine.products.Products;
import com.sda.coffe.machine.products.ProductsReader;
import com.sda.coffe.machine.utils.Consts;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProductsStock {

    private Map<Products, Integer> productsStock;
    ProductsReader productsReader;
    ProductStockReader productStockReader;

    public Map<Products, Integer> getProductsStock() {
        return productsStock;
    }

    public ProductsStock() {
        productsStock = new LinkedHashMap<>();
        productsReader = new ProductsReader();
        productStockReader = new ProductStockReader();
    }

    public void calculateStocs() {
        List<Products> products = productsReader.readProductsFromFile(Consts.PATH_TO_PRODUCTS_FILE);
        List<Integer> stockQuantity = productStockReader.readQuantityFromFile(Consts.PATH_TO_STOCK_FILE);

        for (int i = 0; i < products.size(); i++) {
            productsStock.put(products.get(i), stockQuantity.get(i));
        }
    }

}
