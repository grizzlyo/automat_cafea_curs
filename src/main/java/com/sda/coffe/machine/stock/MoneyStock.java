package com.sda.coffe.machine.stock;

import com.sda.coffe.machine.money.Money;
import com.sda.coffe.machine.money.MoneyReader;
import com.sda.coffe.machine.utils.Consts;

import java.util.LinkedList;
import java.util.List;

public class MoneyStock {

    private List<Money> moneyList;
    MoneyReader moneyReader;

    public MoneyStock() {
        moneyList = new LinkedList<>();
        moneyReader = new MoneyReader();
        calculateStock();
    }

    public List<Money> getMoneyList() {
        return moneyList;
    }

    public void calculateStock(){
       moneyList = moneyReader.readMoneyFromFile(Consts.PATH_TO_MONEY_FILE);
    }


}
