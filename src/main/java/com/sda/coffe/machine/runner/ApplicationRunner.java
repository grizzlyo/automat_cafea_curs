package com.sda.coffe.machine.runner;

import com.sda.coffe.machine.logic.ApplicationLogic;

public class ApplicationRunner {

    public static void main(String[] args) {

        ApplicationLogic logic = new ApplicationLogic();
        logic.runApplication();

    }
}
