package com.sda.coffe.machine.logic;

import com.sda.coffe.machine.display.DisplayData;
import com.sda.coffe.machine.money.Money;
import com.sda.coffe.machine.products.Products;
import com.sda.coffe.machine.stock.MoneyStock;
import com.sda.coffe.machine.stock.ProductsStock;
import com.sda.coffe.machine.utils.Consts;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CoffeeMaker {

    private int coffeeNumber = 0;

    public int getCoffeeNumber() {
        return coffeeNumber;
    }

    public void makeCoffee(ProductsStock productsStock) {

        coffeeNumber++;

        int cantitateCafea = 10;
        int volumApa = 100;
        int cantitateZahar = 5;
        int cantitateLapte = 5;
        int cantitateCacao = 2;

        Map<Products, Integer> products = productsStock.getProductsStock();

        for (Map.Entry<Products, Integer> entry : products.entrySet()) {

            String productName = entry.getKey().getName();
            switch (productName) {
                case "cafea":
                    int cantitate = entry.getValue() - cantitateCafea;
                    productsStock.getProductsStock().put(entry.getKey(), cantitate);
                    break;
                case "apa":
                    int apa = entry.getValue() - volumApa;
                    productsStock.getProductsStock().put(entry.getKey(), apa);
                    break;
                case "zahar":
                    int zahar = entry.getValue() - cantitateZahar;
                    productsStock.getProductsStock().put(entry.getKey(), zahar);
                    break;
                case "lapte":
                    int lapte = entry.getValue() - cantitateLapte;
                    productsStock.getProductsStock().put(entry.getKey(), lapte);
                    break;
                case "cacao":
                    int cacao = entry.getValue() - cantitateCacao;
                    productsStock.getProductsStock().put(entry.getKey(), cacao);
                    break;
                default:
                    DisplayData.displayMessage(Consts.DISPLAY_EXISTING_OPITION);

            }
        }

    }

    public boolean validateQuantity(ProductsStock productsStock) {

        int cantitateCafea = 10;
        int volumApa = 100;
        int cantitateZahar = 5;
        int cantitateLapte = 5;
        int cantitateCacao = 2;

        Map<Products, Integer> products = productsStock.getProductsStock();
        for (Map.Entry<Products, Integer> entry : products.entrySet()) {
            String productName = entry.getKey().getName();

            switch (productName) {
                case "cafea":
                    if (entry.getValue() - cantitateCafea < 0) {
                        return false;
                    }
                    break;
                case "apa":
                    if (entry.getValue() - volumApa < 0) {
                        return false;
                    }
                    break;
                case "zahar":
                    if (entry.getValue() - cantitateZahar < 0) {
                        return false;
                    }
                    break;
                case "lapte":
                    if (entry.getValue() - cantitateLapte < 0) {
                        return false;
                    }
                    break;
                case "cacao":
                    if (entry.getValue() - cantitateCacao < 0) {
                        return false;
                    }
                    break;
                default:
                    DisplayData.displayMessage(Consts.DISPLAY_EXISTING_OPITION);
            }
        }
        return true;
    }

    public List<Money> calculateMoney(int zeceBani,
                                      int cincizeciBani,
                                      int unLEu,
                                      int cinciLEi,
                                      MoneyStock moneyStock) {


        int pret = 200;
        int total = zeceBani * 10 + cincizeciBani * 50 + unLEu * 100 + cinciLEi * 500;
        int rest = total - pret;
        List<Money> moneyRest = new LinkedList<>();

        List<Money> moneyList = moneyStock.getMoneyList();
        int quantity;
        for (Money money : moneyList) {
            switch (money.getDenomination()) {
                case "10bani":
                    quantity = money.getQuantity() + zeceBani;
                    money.setQuantity(quantity);
                    break;
                case "50bani":
                    quantity = money.getQuantity() + cincizeciBani;
                    money.setQuantity(quantity);
                    break;
                case "1leu":
                    quantity = money.getQuantity() + unLEu;
                    money.setQuantity(quantity);
                    break;
                case "5lei":
                    quantity = money.getQuantity() + cincizeciBani;
                    money.setQuantity(quantity);
                    break;
                default:
                    DisplayData.displayMessage(Consts.DISPLAY_EXISTING_OPITION);
            }
        }

        int unLeuRest = rest / 100;
        rest = rest - 100 * unLeuRest;
        int cinciZeciBaniRest = rest / 50;
        rest = rest - 50 * cinciZeciBaniRest;
        int zeceBaniRest = rest / 10;
        rest = rest - 10 * zeceBaniRest;

        for (Money money : moneyStock.getMoneyList()){
            if (money.getDenomination().equals("1leu")){
                quantity = money.getQuantity() - unLeuRest;
                money.setQuantity(quantity);
            }else if (money.getDenomination().equals("50bani")){
                quantity = money.getQuantity() - cinciZeciBaniRest;
                money.setQuantity(quantity);
            }else if (money.getDenomination().equals("10bani")){
                quantity = money.getQuantity() - zeceBaniRest;
                money.setQuantity(quantity);
            }

        }

        Money unLeuMoney = new Money(unLeuRest, "1Leu");
        Money cincizeciBaniMoney = new Money(cinciZeciBaniRest, "50bani");
        Money zeceBaniMoney = new Money(zeceBaniRest, "10bani");

        moneyRest.add(unLeuMoney);
        moneyRest.add(cincizeciBaniMoney);
        moneyRest.add(zeceBaniMoney);

        return moneyRest;
    }

}
