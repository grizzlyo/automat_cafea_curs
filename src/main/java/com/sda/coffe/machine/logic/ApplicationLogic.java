package com.sda.coffe.machine.logic;

import com.sda.coffe.machine.display.DisplayData;
import com.sda.coffe.machine.money.Money;
import com.sda.coffe.machine.stock.MoneyStock;
import com.sda.coffe.machine.stock.ProductsStock;
import com.sda.coffe.machine.utils.Consts;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Scanner;

public class ApplicationLogic {

    final static Logger LOGGER = Logger.getLogger(ApplicationLogic.class);

    private ProductsStock productsStock;
    private MoneyStock moneyStock;
    private CoffeeMaker coffeeMaker;

    public ApplicationLogic() {
        productsStock = new ProductsStock();
        productsStock.calculateStocs();
        moneyStock = new MoneyStock();
        coffeeMaker = new CoffeeMaker();
    }

    public void runApplication() {

        LOGGER.info("Coffee machine started!");

        Scanner scanner = new Scanner(System.in);
        boolean isRunning = true;
        int option;

        DisplayData.displayMessage(Consts.DISPLAY_GENERAL_COFFEE);

        while (isRunning) {

            DisplayData.displayMessage(Consts.DISPLAY_STOCK);
            DisplayData.displayMessage(Consts.DISPLAY_STOCK_MONEY);
            DisplayData.displayMessage(Consts.DISPLAY_PROFIT);
            DisplayData.displayMessage(Consts.DISPLAY_MAKE_COFFEE);
            DisplayData.displayMessage(Consts.DISPLAY_EXIT);
            DisplayData.newLine();
            DisplayData.displayMessage(Consts.DISPLAY_SELECT_OPTION);

            option = scanner.nextInt();

            try {

                switch (option) {
                    case 1:
                        DisplayData.displayStock(productsStock.getProductsStock());
                        break;
                    case 2:
                        DisplayData.displayMoneyStock(moneyStock.getMoneyList());
                        break;
                    case 3:
                        double profit = coffeeMaker.getCoffeeNumber()*60d / 100d ;
                        DisplayData.displayMessage(Consts.DISPLAY_PROFIT_VALUE+profit);
                        break;
                    case 4:
                        if (coffeeMaker.validateQuantity(productsStock)) {

                            DisplayData.displayMessage(Consts.DISPLAY_COFFE_PRICE);
                            DisplayData.displayMessage(Consts.DISPLAY_INSERT_MONEY_MESSAGE);
                            DisplayData.displayMessage(Consts.DISPLAY_INSERT_10_BANI);
                            int zeceBani = scanner.nextInt();
                            DisplayData.displayMessage(Consts.DISPLAY_INSERT_50_BANI);
                            int cincizeciBani = scanner.nextInt();
                            DisplayData.displayMessage(Consts.DISPLAY_INSERT_1_LEU);
                            int unLeu = scanner.nextInt();
                            DisplayData.displayMessage(Consts.DISPLAY_INSERT_5_LEI);
                            int cinciLei = scanner.nextInt();


                            DisplayData.displayMessage(Consts.DISPLAY_COFFEE_IN_PROGRESS);
                            Thread.sleep(100);
                            coffeeMaker.makeCoffee(productsStock);
                            DisplayData.displayMessage(Consts.DISPLAY_COFFEE_FINISHED);
                            DisplayData.displayMessage(Consts.DISPLAY_REST_MESSAGE);
                            List<Money> moneyRestList = coffeeMaker.calculateMoney(zeceBani,
                                    cincizeciBani,
                                    unLeu,
                                    cinciLei,
                                    moneyStock);
                            DisplayData.displayMoneyStock(moneyRestList);

                        } else {
                            DisplayData.displayMessage(Consts.DISPLAY_OUT_OF_STOCK);
                        }
                        break;
                    case 5:
                        isRunning = false;
                        break;
                    default:
                        DisplayData.displayMessage(Consts.DISPLAY_EXISTING_OPITION);

                }

            } catch (InterruptedException ex) {
                LOGGER.error(ex);
                ex.printStackTrace();
            }
        }

    }

}
