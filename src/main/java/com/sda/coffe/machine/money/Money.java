package com.sda.coffe.machine.money;

public class Money {

    private int quantity;
    private String denomination;

    public Money(int quantity, String denomination) {
        this.quantity = quantity;
        this.denomination = denomination;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    @Override
    public String toString() {
        return "Money{" +
                "quantity=" + quantity +
                ", denomination='" + denomination + '\'' +
                '}';
    }
}
