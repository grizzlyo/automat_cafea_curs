package com.sda.coffe.machine.money;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class MoneyReader {

    final static Logger LOGGER = Logger.getLogger(MoneyReader.class);

    public List<Money> readMoneyFromFile (String path){
        LOGGER.info("Entering in method: readMoneyFromFile with path " + path);
        List<Money> moneyList = new LinkedList<>();

        File file = new File(path);

        try (
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader)){

               String line = reader.readLine();

               while (line != null){
                   String[] elements = line.split(" ");

                   Money money = new Money(Integer.parseInt(elements[0]), elements[1]);

                   moneyList.add(money);
                   line = reader.readLine();

               }

        }catch (FileNotFoundException e){
            e.printStackTrace();
            LOGGER.error(e);
        }catch (IOException ex){
            ex.printStackTrace();
            LOGGER.error(ex);
        }

        LOGGER.info("Method readMoneyFromFile have the following output: " + moneyList);
        return moneyList;
    }


}
